from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .forms import StatusForm, SubscribeForm
from .models import Status, Subscriber 
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login, logout

def landingPage(request):
  form = StatusForm()
  response = {}
  status = Status.objects.all()
  response['form'] = form
  response['status'] = status
  return render(request, 'form.html', response)

def coba(request):
  form = StatusForm(request.POST or None)
  if (request.method == 'POST' and form.is_valid()):
    status = request.POST.get('status')
    Status.objects.create(message=status)
    print(status)
    return HttpResponseRedirect('/')
  else:
    return HttpResponseRedirect('/') 

def delete_new(request):
  Status.objects.all().delete()
  return redirect('/')

def profile(request):
  return render(request, 'landingpage.html', {})

def subscribe(request):
  response = {}
  response["forms"] = SubscribeForm()
  return render(request, 'subscribe.html', response)

@csrf_exempt
def validate(request):
  email = request.POST.get("email")
  data = {'not_valid': Subscriber.objects.filter(email__iexact=email).exists()}
  return JsonResponse(data)
  
def success(request):
  submited_form = SubscribeForm(request.POST or None)
  if (submited_form.is_valid()):
    cd = submited_form.cleaned_data
    new_subscriber = Subscriber(name=cd['name'], password=cd['password'], email=cd['email'])
    new_subscriber.save()
    data = {'name': cd['name'],'password':cd['password'], 'email':cd['email']}
    return JsonResponse(data)

# def logout_view(request):
#   logout(request)
#   return HttpResponseRedirect('/books')
    







