from django.conf.urls import url
from django.urls import path
from .views import landingPage,profile,coba,delete_new,subscribe,validate,success
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

app_name = 'appstory6'

urlpatterns = [
	path('home', landingPage, name="form"),
	path('', profile, name="profile"),
	path('addstatus', coba, name="addstatus"),
	path('delete_new', delete_new, name='delete_new'),
	path('subscribe', subscribe, name="subscribe"),
    path('validate', validate),
    path('success', success, name="success")
] 
urlpatterns += staticfiles_urlpatterns()
