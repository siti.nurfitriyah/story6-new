from django.db import models
from django.utils import timezone

class Status(models.Model):
    message = models.CharField(max_length = 300)
    created_at = models.DateTimeField(auto_now_add=True,  null=True, blank=True)

class Subscriber(models.Model):
    name = models.CharField(max_length = 50)
    password = models.CharField(max_length = 20)
    email = models.CharField(max_length = 50)
