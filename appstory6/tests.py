from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import Status, Subscriber
from .forms import StatusForm, SubscribeForm

#functional test
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class Lab6UnitTest(TestCase):
	"""docstring for Lab6UnitTest"""
	def test_lab6_url_is_exist(self):
		response = Client().get('')
		self.assertEqual(response.status_code, 200)

	def test_lab6_use_status(self):
		found = resolve('/home')
		self.assertEqual(found.func, landingPage)

	def test_lab6_has_this_text(self):
		response = Client().get('/home')
		string = response.content.decode('utf8')
		self.assertIn('Hello, Apa Kabar?', string)

	def test_lab6_form_validation_for_blank_item(self):
		form = StatusForm(data={'status': ''})
		self.assertFalse(form.is_valid())

	def test_lab6_post_success_and_render_the_result(self):
		test = ''
		response_post = Client().post('/home', {'status': test})
		self.assertEqual(response_post.status_code, 200)
		response = Client().get('')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)
	
	def test_lab6_using_func(self):
		found = resolve('/')
		self.assertEqual(found.func, profile)

	def test_lab6_profile_has_title(self):
		response = Client().get('/')
		string = response.content.decode('utf8')
		self.assertIn("MY PROFILE", string)
		
	def test_subscribe_has_url(self):
		response = Client().get('/subscribe')
		self.assertEqual(response.status_code, 200)
	def test_subscribe_using_subscribe_func(self):
		found = resolve('/subscribe')
		self.assertEqual(found.func, subscribe)

	def test_subscribe_can_create_object(self):
		Subscriber.objects.create(name="haha", password="hihi", email="hahahihi@gmail.com")
		count_all_stats = Subscriber.objects.all().count()
		self.assertEqual(count_all_stats, 1)
	
	def test_subscribe_form_validation_for_blank_items(self):
		form = SubscribeForm(data={'name': '', 'password': '', 'email':'' })
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['name'],
			["Please enter your name"]
		)
		self.assertEqual(
			form.errors['email'],
			["Please enter a valid email"]
		)
		self.assertEqual(
			form.errors['password'],
			["This field is required."]
        )
	


	



# class Lab7FunctionalTest(TestCase):

#     def setUp(self):
#     	chrome_options = Options()
#     	chrome_options.add_argument('--dns-prefetch-disable')
#     	chrome_options.add_argument('--no-sandbox')        
#     	# chrome_options.add_argument('--headless')
#     	chrome_options.add_argument('disable-gpu')
#     	self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)


#     def tearDown(self):
#         self.selenium.quit()
#         super(Lab7FunctionalTest, self).tearDown()

#     def test_input_todo(self):
#         selenium = self.selenium
#         # Opening the link we want to test
#         selenium.get('http://127.0.0.1:8000/')
#         # find the form element
#         message = selenium.find_element_by_id('id_status')

#         submit = selenium.find_element_by_id('submit')

#         # Fill the form with data
#         time.sleep(4)
#         message.send_keys('Coba Coba')
#         # submitting the form
#         submit.send_keys(Keys.RETURN)
#         time.sleep(4)
#         #check status on page
#         self.assertIn('Coba Coba', selenium.page_source)

#     def test_web_font_is_rum_raisin(self):
#     	selenium = self.selenium
#     	selenium.get('http://fitribelajarppw.herokuapp.com')
#     	font = selenium,find_element_by_tag_name('h1').value_of_css_property('font-family')
#     	self.assertIn("Rum Raisin", font)

#     def test_text_hello_apa_kabar_color(self):
# 	    selenium = self.selenium
# 	    # Opening the link we want to test
# 	    selenium.get('https://fitribelajarppw.herokuapp.com')
# 	    text = selenium.find_element_by_tag_name('h1').value_of_css_property('color')
# 	    self.assertEqual(text, "rgba(0, 0, 102, 1)")
	
# def test_input_status_box_location(self):
#     	selenium = self.selenium
#     	selenium.get('http://fitribelajarppw.herokuapp.com')
#     	statusbox = selenium.find_element_by_id('id_status')
#     	print("loc subtitle:")
#     	print(font.location) 
	
# def test_input_submit_button_location(self):
# 	    selenium = self.selenium
# 	    # Opening the link we want to test
# 	    selenium.get('https://fitribelajarppw.com')
# 	    submit = selenium.find_element_by_tag_name('button')
# 	    print("loc subtitle:")
# 	    print(font.location)
# 	    # self.assertEqual(submit.location['x'], 349)
# 	    # self.assertEqual(submit.location['y'], 142)

  
   






