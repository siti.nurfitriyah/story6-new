from django.shortcuts import render
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.core import serializers
from .forms import SearchForm
from .models import FavBook
from django.contrib.auth import logout as auth_logout
# from app_signup.models import Profile
from django.views.decorators.csrf import csrf_exempt
import requests
import json
# Create your views here.
def search_book(request):
    response = {}
    response['form'] = SearchForm()
    response['data'] = None
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            search = cd['title']
            URL = "https://www.googleapis.com/books/v1/volumes?q=" + search
            get_json = requests.get(URL).json()
            json_dict = json.dumps(get_json)
            response['data'] = json_dict
            if request.user.is_authenticated:
                if 'counter' not in request.session:
                    request.session['counter'] = 0
            
                    print(count)
                if 'nama' not in request.session:
                    request.session['nama'] = request.user.username
                if 'email' not in request.session:
                    request.session['email'] = request.user.email
                response['counter'] = request.session['counter']
    else:
        if request.user.is_authenticated:
            print(request.user.username)
            if 'nama' not in request.session:
                request.session['nama'] = request.user.username
            if 'email' not in request.session:
                request.session['email'] = request.user.email
            if 'counter' not in request.session:
                request.session['counter'] = 0
            count = request.session['counter']
            response['counter'] = count
            print(dict(request.session))

    return render(request, "searchbook.html", response)
def add_fav(request):
    if request.user.is_authenticated:
        # print(dict(request.session))
        # judul = request.GET.get('judul', None)
        # link = request.GET.get('link', None)
        # img = request.GET.get('img', None)
        # author = request.GET.get('author', None)
        # fav = FavBook(title=judul, img=img, link=link, author=author)
        # fav.save()
        print(dict(request.session))
        request.session['counter'] = request.session['counter'] + 1
        print(dict(request.session))
    else:
        pass
    return HttpResponse(request.session['counter'], content_type='application/json')
@csrf_exempt
def del_fav(request):
    if request.user.is_authenticated:
        # judul = request.POST['judul']
        # cui = FavBook.objects.filter(title=judul)
        # print(cui)
        # cui.delete()
        # request.session['counter'] -= 1
        # data = request.session
        # data_json = serializers.serialize('json', data)
        request.session['counter'] = request.session['counter'] - 1
        print(dict(request.session))
        return HttpResponse(request.session['counter'], content_type='aplication/json')
def my_fav(request):
    response = {}
    response['data'] = FavBook.objects.all()
    return render(request, "fav.html", response)	
def logout(request):
    request.session.flush()
    auth_logout(request)
    return HttpResponseRedirect('/books')


